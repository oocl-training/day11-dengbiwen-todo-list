import {createSlice} from '@reduxjs/toolkit'
import {v4 as uuid}  from 'uuid'

export const todoItemsSlice = createSlice({
    name: 'todoItems',
    initialState: {
        todoItems: []
    },
    reducers: {
        setTodoItems: (state, action) => {
            state.todoItems = action.payload
        },
        addTodoItem: (state, action) => {
            state.todoItems.push({
                id: uuid(),
                text: action.payload,
                done: false
            })
        },
    }
})

export const {
    addTodoItem,
    setTodoItems
} = todoItemsSlice.actions

export default todoItemsSlice.reducer