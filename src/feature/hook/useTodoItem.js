import {useDispatch}    from 'react-redux'
import * as todoItemApi from '../../api/todoItemApi'
import {setTodoItems}   from '../todoItemsSlice'

export const useTodoItem = () => {
    const dispatch = useDispatch()
    const loadTodoItems = async () => {
        const response = await todoItemApi.getTodoItems()
        dispatch(setTodoItems(response.data))
    }

    const updateTodoItem = async (id, text, done) => {
        await todoItemApi.updateTodoItem(id, text, done)
        await loadTodoItems()
    }

    const removeTodoItem = async (id) => {
        await todoItemApi.deleteTodoItem(id)
        await loadTodoItems()
    }

    const createTodoItem = async (text, done) => {
        await todoItemApi.createTodoItem(text, done)
        await loadTodoItems()
    }

    const getTodoItemById = async (id) => {
        return await todoItemApi.getTodoItemById(id)
    }

    return {
        loadTodoItems,
        updateTodoItem,
        removeTodoItem,
        createTodoItem,
        getTodoItemById
    }
}