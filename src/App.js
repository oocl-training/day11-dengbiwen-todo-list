import './App.css'
import {Menu}                                              from 'antd'
import {useEffect, useState}                               from 'react'
import {Outlet, useNavigate}                               from 'react-router-dom'
import {HomeOutlined, TeamOutlined, UnorderedListOutlined} from '@ant-design/icons'
import {useTodoItem}                                       from './feature/hook/useTodoItem'

const items = [
    {
        label: 'Home',
        key: '/home',
        icon: <HomeOutlined/>
    },
    {
        label: 'Done List',
        key: '/done',
        icon: <UnorderedListOutlined/>
    },
    {
        label: 'Help',
        key: '/help',
        icon: <TeamOutlined/>
    },
]


function App() {
    const {loadTodoItems} = useTodoItem()

    useEffect(() => {
        loadTodoItems()
    })


    const [currentPage, setCurrentPage] = useState('/home')
    const navigate = useNavigate()
    const onClick = (e) => {
        setCurrentPage(e.key)
        navigate(e.key)
    }
    return (
        <div className = 'App'>
            <Menu onClick = {onClick} selectedKeys = {[currentPage]} mode = 'horizontal' items = {items}></Menu>
            <Outlet></Outlet>
        </div>
    )
}

export default App
