import {configureStore} from '@reduxjs/toolkit'
import todoItemsReducer from '../feature/todoItemsSlice'

export default configureStore({
    reducer: {todoItems: todoItemsReducer}
})