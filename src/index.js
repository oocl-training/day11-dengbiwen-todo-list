import React                                 from 'react'
import ReactDOM                              from 'react-dom/client'
import './index.css'
import App                                   from './App'
import store                                 from './app/store'
import {Provider}                            from 'react-redux'
import {createBrowserRouter, RouterProvider} from 'react-router-dom'
import ErrorPage                             from './page/ErrorPage'
import HomePage                              from './page/HomePage'
import DoneListPage                          from './page/DoneListPage'
import DoneDetail                            from './component/doneList/DoneDetail'
import HelpPage                              from './page/HelpPage'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App></App>,
        errorElement: <ErrorPage></ErrorPage>,
        children: [
            {
                path: '/',
                element: <HomePage></HomePage>
            },
            {
                path: '/home',
                element: <HomePage></HomePage>
            },
            {
                path: '/done',
                element: <DoneListPage></DoneListPage>
            },
            {
                path: '/done/:id',
                element: <DoneDetail></DoneDetail>
            },
            {
                path: '/help',
                element: <HelpPage></HelpPage>
            }
        ]
    }
])


const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(<React.StrictMode>
    <Provider store = {store}>
        <RouterProvider router = {router}></RouterProvider>
    </Provider>,
</React.StrictMode>)
