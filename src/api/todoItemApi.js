import api from './api'


export const getTodoItems = () => {
    return api.get('/todoItems')
}

export const getTodoItemById = (id) => {
    return api.get(`/todoItems/${id}`)
}

export const updateTodoItem = (id, text, done) => {
    return api.put(`/todoItems/${id}`, {
        text,
        done
    })
}


export const deleteTodoItem = (id) => {
    return api.delete(`/todoItems/${id}`)
}

export const createTodoItem = (text, done) => {
    return api.post('/todoItems', {
        text,
        done
    })
}