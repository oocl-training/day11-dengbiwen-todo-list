import TodoItem                           from './TodoItem'
import {useSelector}                      from 'react-redux'
import {useState}                         from 'react'
import {useTodoItem}                      from '../../feature/hook/useTodoItem'
import {Drawer, Input, Modal, Pagination} from 'antd'

const TodoGroup = () => {
    const {
        updateTodoItem,
        getTodoItemById
    } = useTodoItem()
    const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false)
    const [todoItemClicked, setTodoItemClicked] = useState({})

    const [isDetailDrawerOpen, setIsDetailDrawerOpen] = useState(false)
    const [todoItemShowed, setTodoItemShowed] = useState({
        id: '',
        text: '',
        done: ''
    })

    const [page, setPage] = useState(1)
    const todoItems = useSelector(state => state.todoItems.todoItems)


    const handleShowUpdateModalButtonClick = (todoItemClick) => {
        setIsUpdateModalOpen(!isUpdateModalOpen)
        setTodoItemClicked(todoItemClick)
    }
    const handleCancelUpdateButtonClick = () => {
        setIsUpdateModalOpen(!isUpdateModalOpen)
    }
    const handleEnsureUpdateButtonClick = async () => {
        await updateTodoItem(todoItemClicked.id, todoItemClicked.text, todoItemClicked.done)
        setIsUpdateModalOpen(!isUpdateModalOpen)
    }

    const handleCancelDrawerClick = () => {
        setIsDetailDrawerOpen(!isDetailDrawerOpen)
    }

    const handleOpenDrawerButtonClick = async (id) => {
        const response = await getTodoItemById(id)
        setTodoItemShowed(response.data)
        setIsDetailDrawerOpen(!isDetailDrawerOpen)
    }


    const handlePageChange = (page) => {
        setPage(page)
        console.log(page)
    }
    return (<div>
        <Modal title = 'Update TodoItem Text' open = {isUpdateModalOpen} onOk = {handleEnsureUpdateButtonClick}
               onCancel = {handleCancelUpdateButtonClick}>
            <Input value = {todoItemClicked.text} onChange = {e => setTodoItemClicked({
                ...todoItemClicked,
                text: e.target.value
            })}/>
        </Modal>

        <Drawer title = 'Basic Drawer' placement = 'right' onClose = {handleCancelDrawerClick}
                open = {isDetailDrawerOpen}>
            <p>ID: {todoItemShowed.id}</p>
            <p>Text: {todoItemShowed.text}</p>
            <p>Done State: {todoItemShowed.done.toString()}</p>
        </Drawer>

        {[...todoItems].reverse().slice((page - 1) * 10, page * 10).map((todoItem) => <TodoItem key = {todoItem.id}
                                                                                                text = {todoItem.text}
                                                                                                id = {todoItem.id}
                                                                                                done = {todoItem.done}
                                                                                                onShowUpdateModalButtonClick = {handleShowUpdateModalButtonClick}
                                                                                                onShowDetailDrawerButtonClick = {handleOpenDrawerButtonClick}></TodoItem>)}


        <Pagination defaultCurrent = {1} total = {todoItems.length} onChange = {handlePageChange}/>
    </div>)
}

export default TodoGroup

