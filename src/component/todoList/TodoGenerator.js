import {useState}      from 'react'
import {Button, Input} from 'antd'
import {useTodoItem}   from '../../feature/hook/useTodoItem'

export default function TodoGenerator() {
    const [textInputted, setTextInputted] = useState('')
    const {createTodoItem} = useTodoItem()

    const handleAddTodoItemButtonClicked = async () => {
        if (textInputted !== '') {
            await createTodoItem(textInputted, false)
            setTextInputted('')
        }
    }
    const changeTextInputted = (event) => {
        setTextInputted(event.target.value)
    }

    return (<div className = 'todo-generator'>
        <Input type = 'text'
               value = {textInputted}
               onChange = {changeTextInputted}/>
        <Button type = 'primary' onClick = {handleAddTodoItemButtonClicked}>add</Button>
    </div>)
}