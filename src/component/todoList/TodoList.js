import TodoGroup     from './TodoGroup'
import TodoGenerator from './TodoGenerator'

const TodoList = () => {
    return (<div className = 'todo-list'>
        <span>Todo List</span>
        <TodoGenerator></TodoGenerator>
        <TodoGroup></TodoGroup>
    </div>)
}
export default TodoList