import {Button}                                    from 'antd'
import {DeleteOutlined, EditOutlined, EyeOutlined} from '@ant-design/icons'
import {useTodoItem}                               from '../../feature/hook/useTodoItem'

const TodoItem = (props) => {
    const {
        updateTodoItem,
        removeTodoItem
    } = useTodoItem()
    const {
        id,
        text,
        done,
        onShowUpdateModalButtonClick,
        onShowDetailDrawerButtonClick
    } = props

    const className = done ? 'font-without-line-through' : 'font-with-line-through'

    const handleToggleDoneButtonClick = async () => {
        await updateTodoItem(id, text, !done)
    }

    const handleRemoveTodoItemButtonClick = async () => {
        if (window.confirm('Are you sure you wish to remove this todoItem')) {
            await removeTodoItem(id)
        }
    }


    return (<div className = 'todo-item'>
        <p className = {className} onClick = {handleToggleDoneButtonClick}>{text}</p>
        <Button type = 'default' onClick = {handleRemoveTodoItemButtonClick}><DeleteOutlined/></Button>
        <Button type = 'default' onClick = {() => onShowUpdateModalButtonClick({
            id,
            text,
            done
        })}><EditOutlined/></Button>
        <Button type = 'default' onClick = {() => onShowDetailDrawerButtonClick(id)}><EyeOutlined/></Button>
    </div>)
}

export default TodoItem