import DoneItem      from './DoneItem'
import {useSelector} from 'react-redux'


const DoneGroup = () => {


    const doneItems = useSelector(state => state.todoItems.todoItems).filter(item => item.done)
    return (<div>
        {doneItems.map((doneItem) => <DoneItem key = {doneItem.id} text = {doneItem.text}
                                               id = {doneItem.id} done = {doneItem.done}></DoneItem>)}
    </div>)
}

export default DoneGroup

