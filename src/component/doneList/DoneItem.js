import {useNavigate} from 'react-router-dom'

const DoneItem = (props) => {
    const navigate = useNavigate()
    const {
        id,
        text,
        done
    } = props
    const className = done ? 'font-with-line-through' : 'font-without-line-through'
    return (<div className = 'todo-item'>
        <p className = {className} onClick = {() => navigate('/done/' + id)}>{text}</p>
    </div>)
}

export default DoneItem