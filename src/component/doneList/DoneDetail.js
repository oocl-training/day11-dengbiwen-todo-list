const {useParams} = require('react-router-dom')
const {useSelector} = require('react-redux')
const DoneDetail = () => {
    const {id} = useParams()
    const todoItem = useSelector(state => state.todoItems.todoItems).find(item => item.id === id)
    return (<div>
        <span>Done detail</span>
        <div>{todoItem?.id}</div>
        <div>{todoItem?.text}</div>
    </div>)
}

export default DoneDetail