## ORID

---

### O (Objective)

`What did we learn today? What activities did you do? What scenes have impressed you?`

1. router
2. axios
3. ant design

---

### R (Reflective)

`Please use one word to express your feelings about today's class.`

+ strange

---

### I (Interpretive)

`What do you think about this? What was the most meaningful aspect of this activity?`

+ router in react

  The core is the router table. With the router table, we can use navigate to jump a page but not refresh browser.

  The router table also looks like vue router. Note that pathvariable need use form like ":id".

+  axios in react

   In order to reuse the baseUrl, we need package api.js. Also, in order to reuse the api of resourse, we can package its http RESTful method. In the end, we can package a hook to use axios to maintain redux.

+  ant design in react

   A convenient library to help us design a responsive page.


---

### D (Decisional)

`Where do you most want to apply what you have learned today? What changes will you make?`

+ Router is a tool that help us guide page but not refresh browser.
+ Axios is more convenient than ajax, when requesting back end.
+ Ant Design can help us develop web page without pay more attention on write css and multi terminal compatibility issues.
+ I will use them to improve the efficiency and quality of my webpage development