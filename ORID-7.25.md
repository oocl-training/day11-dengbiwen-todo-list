## ORID

---

### O (Objective)

`What did we learn today? What activities did you do? What scenes have impressed you?`

1. Redux

---

### R (Reflective)

`Please use one word to express your feelings about today's class.`

+ strange

---

### I (Interpretive)

`What do you think about this? What was the most meaningful aspect of this activity?`

+ Redux is a container of data stored in memory, which provider some global methods to manipulate data. We can define reducers to personalize our method to add or remove data. When defining method in reducers, we can receive parameters from `action.payload` and update our data in container by assigning `state.ourDataName`. 

+ Noted that the update operation of our data isn't assigning new value to our data, which is actually a process of copy old value and replace with new value.

+ Redux and Vuex are very similar. They all have state. The features of redux look like the modules of vuex. The useSelector of redux looks like the getters of vuex. The reducers of redux look like the mutations of vuex, but vuex need use actions to commit the mutations.

+ In my opinion, there is almost no difference between Vuex and Redux, but the main difference is between Vue and React.

---

### D (Decisional)

`Where do you most want to apply what you have learned today? What changes will you make?`

+ When we need to transfer value between deep components, we can use redux to store global data.
+ replace define state in function component when manipulate a amount of data or deep components.

