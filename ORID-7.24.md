## ORID

---

### O (Objective)

`What did we learn today? What activities did you do? What scenes have impressed you?`

1. Spring Boot
2. Html, css
3. react

---

### R (Reflective)

`Please use one word to express your feelings about today's class.`

strange

---

### I (Interpretive)

`What do you think about this? What was the most meaningful aspect of this activity?`

1. Code review

   The transfer of object such as DTO and VO, need be included by Service Layer. The transfer belongs to service, because we want to transfer to this Object sometimes but want to transfer other Object sometimes. It is a process of our business. 

2. spring boot summarize

   This is my general understanding, there may be a mistake. It is my SpringBoot is different from SpringMVC. Maybe the controller of springboot looks like the behavior springMVC handle Servlet, but it don't mean that springMVC belong to SpringBoot. They just all use spring to handle servlet.

3. html, css review

4. react

   Learning the value transfered among components has some difficulties. It's the first time I saw this form of parameter transfer. We should use state in father component to let brother communicating in React. However, we usually transfer value by update event in vue. For example, father component can receive a event and handle it after som component occur a event such updating data, clicking button and so on. In terms of the react currently I learned, the method of transfering value, react win.

---

### D (Decisional)

`Where do you most want to apply what you have learned today? What changes will you make?`

Try my best to transition from Vue to React.